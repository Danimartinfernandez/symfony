# README #

### Una captura de pantalla de l'aplicació en marxa a local i després al labs ###

* Captura del symfony en local.

![local](https://bytebucket.org/Danimartinfernandez/symfony/raw/7c39fb529cb2609d88e52bb6fcab61eea536868f/Symfony_local.png)

* Captura del symfony al labs

![labs](https://bytebucket.org/Danimartinfernandez/symfony/raw/5cf114e1817a86b967cd1382b4282d5c97bbc479/symfonyLabs.png)

### On es vegi l'imatge amb una referència a la pàgina del manual de symfony que heu seguit ###

* [Symfony](https://symfony.com/doc/3.4/setup/web_server_configuration.html)
* [Github](https://github.com/symfony/demo/blob/master/README.md)

### Que expliqui molt breument els errors del document d'instal·lació (si n'hi ha) que heu seguit, així com els punts que trobeu més difícils. ###

* L'error que he tingut, ha sigut a causa dels permisos de la carpeta /var del projecte symfony, perque no tenia els permisos necessaris.

* Captura de l'error que s'havia excedit la quota de disc

![Error](https://bytebucket.org/Danimartinfernandez/symfony/raw/68f1d7c5cb96053d5793d978109d21bb6e7ded91/Error_cuota.png)

### Expliqueu si hi ha alguna diferència entre local i labs.iam.cat? ###

* No he pogut fer-ho al labs, em donava l'error de que s'havia superat la quota.

### Heu de lliurar la URL (clicable) d'aquesta pàgina. Assegureu-vos de que està publica a la web!!!! (proveu-ho des d'una finestra d'incògnit ###


